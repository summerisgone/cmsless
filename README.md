# Development Setup


1. Create virtualenv:

    virtualenv --system-site-packages .env

2. Install requirements:

    source .env/bin/activate && pip install -r requirements.txt

3. Run web project:

    python manage.py runserver

3. Run rq worker:

    python manage.py rqworker


## Application keys

To export app keys, run dashboard `loadkeys` command:

    export DROPBOX_KEY=<app key>
    export DROPBOX_SECRET=<app secret>
    python manage.py loadkeys

To work with Amazon S3 you'll need secret key and access key.
They are looked up in environment variables ``secret_key`` and ``access_key``

If you're using s3cmd, you can import them:

    export `cat ~/.s3cfg | grep key | sed -e 's/\s//g'`

## Requirements

You need JRE installed in order ``s3_website`` tool works.

    # apt-get install openjdk-7-jre -y