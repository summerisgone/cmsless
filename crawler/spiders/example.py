# -*- coding: utf-8 -*-
import scrapy
import os
from urlparse import urljoin, urlparse
from spider.items import Item
from spider.content import extract
import lxml.html
from scrapy.utils.project import get_project_settings


settings = get_project_settings()

class WebsiteSpider(scrapy.Spider):
    name = "site"
    allowed_domains = [settings.get('DEFAULT_DOMAIN')]
    start_urls = (
        'http://{0}/'.format(settings.get('DEFAULT_DOMAIN')),
    )

    def parse(self, response):
        if type(response) is scrapy.http.response.html.HtmlResponse:
            doc_data = {'title': ''.join(response.xpath('//title/text()').extract()).replace('"', '\\"'),
                        'filename': ''.join(os.path.splitext(os.path.basename(response.url))).replace('"', '\\"'),
                        'path': urlparse(response.url).path,
                        'keywords': ''.join(response.xpath('//meta[@name="keywords"]/@content').extract()).replace('"', '\\"'),
                        'description': ''.join(response.xpath('//meta[@name="description"]/@content').extract()).replace('"', '\\"'),
                        'image_urls': [],
                        'content': extract(response.body)}

            doc = lxml.html.document_fromstring(response.body)
            images = doc.xpath('//img')
            for img in images:
                img.make_links_absolute(base_url=response.url)
                doc_data['image_urls'].append(img.attrib['src'])

            yield Item(**doc_data)

            for url in response.xpath('//a/@href').extract():
                url = urljoin(response.url, url)
                if not url.startswith('http'):
                    continue
                yield scrapy.Request(url, callback=self.parse)