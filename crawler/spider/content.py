from readability.readability import Document


def extract(html):
    try:
        doc = Document(html)
        return doc.summary()
    except Exception:
        return html
