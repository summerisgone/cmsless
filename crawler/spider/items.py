# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    filename = scrapy.Field()
    path = scrapy.Field()
    description = scrapy.Field()
    keywords = scrapy.Field()

    image_urls = scrapy.Field()
    images = scrapy.Field()