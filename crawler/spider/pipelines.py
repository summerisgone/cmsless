# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from PIL import Image
from html2text import HTML2Text
from os.path import join, exists, dirname, abspath, splitext, basename
from os import makedirs
from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.utils.project import get_project_settings
from StringIO import StringIO
from urlparse import urlparse


settings = get_project_settings()

class MyImagesPipeline(ImagesPipeline):

    def convert_image(self, image, size=None):
        if image.format == 'PNG' and image.mode == 'RGBA':
            background = Image.new('RGBA', image.size, (255, 255, 255))
            background.paste(image, image)
            image = background.convert('RGB')
        elif image.mode != 'RGB':
            image = image.convert('RGB')

        if size:
            image = image.copy()
            image.thumbnail(size, Image.ANTIALIAS)

        buf = StringIO()
        image.save(buf, image.format if image.format == 'PNG' else 'JPEG')
        return image, buf

    def image_downloaded(self, response, request, info):
        return super(MyImagesPipeline, self).image_downloaded(response, request, info)

    def image_key(self, url):
        return urlparse(url).path

class FormatPipeline(object):

    @staticmethod
    def unwrap_tag(content, tag):
        content = content.strip()
        if content.startswith('<{0}>'.format(tag)):
            content = content.split('<{0}>'.format(tag), 1)[1]
        if content.endswith('</{0}>'.format(tag)):
            content = content.rsplit('</{0}>'.format(tag), 1)[0]
        return content

    def process_item(self, item, spider):
        item['content'] = self.unwrap_tag(item['content'], 'tr')
        item['content'] = self.unwrap_tag(item['content'], 'td')
        return item

class DocumentPipeline(object):
    
    def add_jekyll_meta(self, md, item):
        meta = u"""---
layout: page
title: "{title}"
description: "{description}"
keywords: "{keywords}"
permalink: "{path}"
---

""".format(**item)
        return meta + md
    
    def relative_links(eslf, md):
        return md.replace('http://{0}/'.format(settings.get('DEFAULT_DOMAIN')), '/')

    @staticmethod
    def unwrap_tag(content, tag):
        content = content.strip()
        if content.startswith('<{0}>'.format(tag)):
            content = content.split('<{0}>'.format(tag), 1)[1]
        if content.endswith('</{0}>'.format(tag)):
            content = content.rsplit('</{0}>'.format(tag), 1)[0]
        return content

    def process_item(self, item, spider):
        filename = join(abspath(settings.get('DEFAULT_OUTPUT')), item['path'][1:])
        if item['path'].endswith('/'):
            filename = join(filename, 'index.html')
        if not exists(dirname(filename)):
            makedirs(dirname(filename))

        filename = splitext(filename)[0] + '.md'
        with open(filename, 'w') as f:
            h = HTML2Text(bodywidth=0)
            h.bypass_tables=True
            md = h.handle(item['content'])
            # md = item['content']
            # relative links
            md = self.relative_links(md)
            md = self.unwrap_tag(md, 'tr')
            md = self.unwrap_tag(md, 'td')
            md = self.add_jekyll_meta(md, item)
            f.write(md.encode('utf-8'))
        return item
