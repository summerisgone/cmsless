import os
from django.conf import settings


class SiteService(object):

    def __init__(self, website):
        self.website = website

    @staticmethod
    def get_preview_path(website):
        return os.path.join(settings.USER_PREVIEWS_PATH, website.get_unique_id())

    @staticmethod
    def get_content_path(website):
        return os.path.join(settings.USER_CONTENT_PATH, str(website.pk))

    @staticmethod
    def get_preview_url(website):
        return "http://%s.%s" % (website.get_unique_id(), settings.DOMAIN)

    @staticmethod
    def get_renderer(website):
        from renderer.jekyll_render import JekyllRenderer
        return JekyllRenderer(website)

    @staticmethod
    def get_preview_publisher(website):
        from publisher.local import LocalPublisher
        return LocalPublisher(website)

    @staticmethod
    def get_publisher(website):
        from publisher.amazon import AmazonPublisher
        return AmazonPublisher(website)

    @staticmethod
    def get_sync(website):
        from sync.local_sync import LocalSync
        return LocalSync(website)
