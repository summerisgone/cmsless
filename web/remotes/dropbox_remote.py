from dropbox.client import DropboxClient
from os.path import join
from base import BaseRemote


class DropboxRemote(BaseRemote):

    def __init__(self, access_token, root='/'):
        super(DropboxRemote, self).__init__()
        self.root = root
        self.access_token = access_token
        self.__client = DropboxClient(self.access_token)

    def _remove_root(self, path):
        return ''.join(path.split(self.root, 1))

    def _add_root(self, path):
        if path is None:
            return self.root
        return join(self.root, path.lstrip('/'))

    def list(self, root=None):
        folder_metadata = self.__client.metadata(self._add_root(root), list=True)
        for item in folder_metadata['contents']:
            yield {
                'path': self._remove_root(item['path']),
                'is_dir': item['is_dir']
            }

    def sync(self, cursor=None, prefix=None):
        d = self.__client.delta(cursor=cursor, path_prefix=self._add_root(prefix).rstrip('/'))
        yield d['cursor']
        for path, item in d['entries']:
            if item is None:
                yield {
                    'path': path,
                    'deleted': True
                }
            else:
                if not item['is_dir']:
                    yield {
                        'path': self._remove_root(path),
                        'is_dir': item['is_dir']
                    }

    def read_file(self, path):
        with self.__client.get_file(self._add_root(path)) as f:
            return f.read()

    def remove(self, path):
        self.__client.file_delete(self._add_root(path))

    def write_file(self, path, content):
        self.__client.put_file(self._add_root(path), content, overwrite=True)
