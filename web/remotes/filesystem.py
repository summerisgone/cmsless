import os
from os.path import join, isdir, relpath, dirname
from shutil import rmtree
from base import BaseRemote
from utils import ensure_dir


class FileSystemRemote(BaseRemote):

    def __init__(self, chroot):
        super(FileSystemRemote, self).__init__()
        self.chroot = chroot

    def _get_dir(self, *args):
        return join(self.chroot, *args)

    def list(self, root=''):
        for item in os.listdir(self._get_dir(root)):
            yield {
                'path': relpath(self._get_dir(root, item), self.chroot),
                'is_dir': isdir(self._get_dir(root, item))
            }

    def initialize(self):
        ensure_dir(self._get_dir(''))

    def read_file(self, path):
        with open(self._get_dir(path)) as f:
            return f.read()

    def remove(self, path):
        path = self._get_dir(path)
        if os.path.exists(path):
            if isdir(path):
                rmtree(path)
            else:
                os.remove(path)

    def write_file(self, path, content):
        ensure_dir(dirname(self._get_dir(path)))
        with open(self._get_dir(path), 'w') as f:
            return f.write(content)
