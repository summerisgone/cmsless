from os.path import join
import os
import re
import yaml


class BaseRemote(object):

    def list(self, root=None):
        raise NotImplemented

    def read_file(self, path):
        raise NotImplemented

    def remove(self, path):
        raise NotImplemented

    def write_file(self, path, content):
        raise NotImplemented

    def sync(self, cursor=None, prefix='/'):
        # default implementation without any caching
        yield None
        for root, dirs, files in self.walk():
            for file_obj in files:
                path_join = join(root, file_obj['path']) if root else file_obj['path']
                file_obj['path'] = path_join
                yield file_obj

    def walk(self, root=None):
        folders = []
        files = []
        for item in self.list(root):
            if item['is_dir']:
                folders.append(item)
            else:
                files.append(item)
        yield (root, folders, files)

        for folder in folders:
            for res in self.walk(folder['path']):
                yield res
