from renderer.base import BaseRenderer


class NoneRenderer(BaseRenderer):
    def render(self):
        return self.copy_data_files()
