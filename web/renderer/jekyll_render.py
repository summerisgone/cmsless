import subprocess
from os.path import join
from renderer.base import BaseRenderer
import logging


class JekyllRenderer(BaseRenderer):

    def render(self):
        root = self.copy_data_files()
        p = subprocess.Popen("jekyll build",
                             shell=True,
                             stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             cwd=root)
        out, err = p.communicate()
        if err != '':
            logging.getLogger().error(err)
        if p.returncode != 0:
            # shutil.rmtree(root) ?
            raise Exception(
                "Jekyll running error: {0}-{1}-{2}-{3}".format("jekyll build", out, err, p.returncode))
        logging.getLogger().debug("rendered site in {0}".format(root))
        return join(root, "_site")
