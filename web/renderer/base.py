from tempfile import mkdtemp
from shutil import copytree
from os.path import join, exists, isdir

from common.site_service import SiteService


class NoContentException(Exception):
    pass


class BaseRenderer(SiteService):

    def copy_data_files(self):
        if not self.has_data():
            raise NoContentException("No content in data folder")
        unique_id = self.website.get_unique_id()
        temp_root = mkdtemp(prefix=unique_id + '-')
        temp_root = join(temp_root, unique_id)
        copytree(SiteService.get_content_path(self.website), temp_root)
        return temp_root

    def has_data(self):
        content_path = SiteService.get_content_path(self.website)
        return exists(content_path) and isdir(content_path)

    def render(self):
        """Should return temporary folder with rendered html files"""
        raise NotImplementedError
