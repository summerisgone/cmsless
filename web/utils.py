# -*- coding: utf-8 -*-
import os
import hashlib
import pickle


def ensure_dir(folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)


def hash_folder(folder_name):
    data = []
    for root, folders, files in os.walk(folder_name):
        for file_name in files:
            path_join = os.path.join(root, file_name)
            data.append({
                'path': path_join,
                'time': os.stat(path_join).st_ctime
            })
    z = pickle.dumps(data)
    return hashlib.md5(z).hexdigest()
