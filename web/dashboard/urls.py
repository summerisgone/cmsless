from django.conf.urls import patterns, url

from views import job_status, dropbox_webhook, Frontpage, \
    WebsiteCreate, WebsiteDetail, WebsiteUpdate, WebsiteDelete, WebsiteList, \
    WebsiteSync, WebsitePreview, WebsitePublish, WebsiteStencil, WebsiteEditFile



urlpatterns = patterns('',
    url(r'^webhook$', dropbox_webhook),
    url(r'^$', Frontpage.as_view(), name='frontpage'),
    url(r'^site$', WebsiteList.as_view(), name='site_list'),
    url(r'^job/(?P<queue>\w+)', job_status, name='job_status'),
    url(r'^site/add/$', WebsiteCreate.as_view(), name='site_create'),
    url(r'^site/(?P<pk>\w+)/$', WebsiteDetail.as_view(), name='site_detail'),
    url(r'^site/(?P<pk>\w+)/delete$', WebsiteDelete.as_view(), name='site_delete'),
    url(r'^site/(?P<pk>\w+)/update$', WebsiteUpdate.as_view(), name='site_update'),
    url(r'^site/(?P<pk>\w+)/sync', WebsiteSync.as_view(), name='site_sync'),
    url(r'^site/(?P<pk>\w+)/preview$', WebsitePreview.as_view(), name='site_preview'),
    url(r'^site/(?P<pk>\w+)/publish', WebsitePublish.as_view(), name='site_publish'),
    url(r'^site/(?P<pk>\w+)/files/(?P<path>.*)$', WebsiteEditFile.as_view(), name='site_file_editor'),
    url(r'^site/(?P<pk>\w+)/stencil$', WebsiteStencil.as_view(), name='site_stencil'),
)
