from allauth.socialaccount.models import SocialApp
from django.db import models
from django.core.urlresolvers import reverse
import requests
from requests_oauthlib import OAuth1
from remotes import DropboxRemote, FileSystemRemote
import os.path
from django.conf import settings


class Remotes:
    Dropbox = 'dropbox'  # same as allauth provider name
    FileSystem = 'fs'


REMOTE_CHOICES = (
    (Remotes.Dropbox, 'Dropbox'),
    (Remotes.FileSystem, 'Local File System'),
)


def get_dropbox_key(app_key, app_secret, token, token_secret):
    auth = OAuth1(app_key, app_secret, token, token_secret)
    resp = requests.post('https://api.dropbox.com/1/oauth2/token_from_oauth1', auth=auth)
    return resp.json()['access_token']


class SyncCursor(models.Model):
    cursor = models.CharField(max_length=255)
    hash = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    website = models.ForeignKey('WebSite')


class WebSite(models.Model):
    user = models.ForeignKey('auth.User')
    domain = models.CharField(max_length=255)
    remote = models.CharField(choices=REMOTE_CHOICES, max_length=255)

    def get_absolute_url(self):
        return reverse('site_detail', args=[self.pk])

    def get_remote_instance(self):
        if self.remote == Remotes.Dropbox:
            social = self.user.socialaccount_set.filter(provider=self.remote).first()
            if social and 'oauth2key' in social.extra_data:
                return DropboxRemote(social.extra_data['oauth2key'], '/' + self.get_unique_id())
        if self.remote == Remotes.FileSystem:
            return FileSystemRemote(os.path.join(settings.USER_SITES_STORAGE, str(self.user.pk), str(self.pk)))

    def _dropbox_create_hook(self):
        """Cache OAuth2 key in extra data"""
        social = self.user.socialaccount_set.filter(
            provider=Remotes.Dropbox).first()
        if social and 'oauth2key' not in social.extra_data:
            token = social.socialtoken_set.first()
            provider = SocialApp.objects.get(provider=Remotes.Dropbox)
            key = get_dropbox_key(provider.client_id, provider.secret, token.token, token.token_secret)
            social.extra_data['oauth2key'] = key
            social.save()

    def _fs_create_hook(self):
        remote = self.get_remote_instance()
        remote.initialize()

    def initialize_remote(self):
        if self.remote == Remotes.Dropbox:
            self._dropbox_create_hook()
        if self.remote == Remotes.FileSystem:
            self._fs_create_hook()

    def __unicode__(self):
        return self.domain

    def get_unique_id(self):
        return "%s%d" % (self.domain.replace(".", ""), self.pk)
