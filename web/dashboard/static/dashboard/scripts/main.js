$(function () {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function pollSubmit(target) {
        var _submit;

        function loading(status) {
            var spinner = target.find('.js-spinner');
            if (!spinner.length) {
                spinner = $('<div class="loader js-spinner" />').appendTo(target);
            }
            spinner.text(status);
        }

        function hideLoading() {
            target.find('.js-spinner').remove();
        }

        function _finally() {
            enable();
            hideLoading();
        }

        function error(message) {
            _finally();
            var badge = $('<span class="js-label label label-danger"><i class="glyphicon glyphicon-warning-sign" /> failed</span>')
                .appendTo(target);
            badge.popover({
                content: '<pre>' + message + '</pre>',
                html: true,
                title: 'task failed'
            });
        }

        function success() {
            _finally();
            $('<span class="js-label label label-success"><i class="glyphicon glyphicon-check" /> OK</span>')
                .appendTo(target);
        }

        function clean() {
            target.find('.js-spinner').remove();
            target.find('.js-label').popover('destroy');
            target.find('.js-label').remove();
        }

        function start() {
            clean();
            disable();
            loading('sending request');
        }

        function disable() {
            _submit = target.find(':submit').prop('disabled', true);
        }

        function enable() {
            _submit.prop('disabled', false);
        }

        var submit = Rx.Observable.fromEvent(target, 'submit')
            .do(function (e) {
                e.preventDefault();
                start();
            })
            .map(function (e) {
                var $form = $(e.target);
                return {
                    data: $form.serializeObject(),
                    method: $form.attr('method'),
                    url: $form.prop('action')
                };
            });

        submit.subscribe(function (data) {
            var poll = Rx.Observable.fromPromise($.ajax(data))
                    .expand(function (data) {
                        var url = '/job/default?task_id=' + data.task_id;
                        return Rx.Observable.fromPromise($.getJSON(url));
                    })
                    .map(function (data) {
                        if (data.status == 'failed') {
                            throw new Error(data.exc_info);
                        }
                        return data;
                    })
                    .takeWhile(function (data) {
                        return data.status == 'started' || data.status == 'queued';
                    })
                ;

            poll.subscribe(
                function (data) {
                    loading(data.status + '(' + data.description + ')');
                },
                function (err) {
                    error(err.message);
                },
                function () {
                    success();
                });
        });

        submit.subscribe(function onNext() {
            disable();
        });


    }

    $.fn.pollSubmit = function () {
        return this.each(function () {
            pollSubmit($(this));
        })
    };
    $('form').pollSubmit();


});