from common.site_service import SiteService
from dashboard.models import WebSite
from publisher.base import NoPreviewException
from renderer.base import NoContentException
from django_rq import job
import logging
import os

@job
def sleep_n(n):
    for i in range(n):
        from time import sleep
        sleep(1)
        logging.info('slept {0} s'.format(i+1))
    return n

@job
def sync_site(site_id):
    logging.info("Sync site {0}".format(site_id))
    sync = SiteService.get_sync(WebSite.objects.get(pk=site_id))
    sync.sync()


@job
def preview_site(site_id):
    logging.info("Rendering site {0} preview ".format(site_id))
    renderer = SiteService.get_renderer(WebSite.objects.get(pk=site_id))
    try:
        path = renderer.render()
    except NoContentException:
        logging.info("...no content is up to date. Need sync")
        sync_site(site_id)
        path = renderer.render()
    publisher = SiteService.get_preview_publisher(WebSite.objects.get(pk=site_id))
    url = publisher.publish(path)
    logging.info("Preview ready: {0}".format(url))
    return url


@job
def publish_site(site_id):
    logging.info("Publishing site {0} preview ".format(site_id))
    renderer = SiteService.get_renderer(WebSite.objects.get(pk=site_id))
    try:
        path = renderer.render()
    except NoContentException:
        logging.info("...no content is up to date. Need sync")
        preview_site(site_id)
        path = renderer.render()

    publisher = SiteService.get_publisher(WebSite.objects.get(pk=site_id))
    try:
        url = publisher.publish(path)
    except NoPreviewException:
        logging.info("...preview is not ready. Rendering one for publishing")
        preview_site(site_id)
        url = publisher.publish(path)
    logging.info("Site published: {0}".format(url))
    return url

@job
def copy_stencil(site_id, src):
    logging.info("Copy stencil {0} to site {1}".format(src, site_id))
    website = WebSite.objects.get(pk=site_id)
    remote = website.get_remote_instance()
    for root, folders, files in os.walk(src):
        for filename in files:
            full_file_path = os.path.join(root, filename)
            relative_file_path = os.path.relpath(full_file_path, src)
            with open(full_file_path) as f:
                remote.write_file(relative_file_path, f.read())
    logging.info("Stencil {0} copied".format(src))
