# -*- coding: utf8 -*-
from django import forms
from django.conf import settings
import os.path


class StencilForm(forms.Form):

    @staticmethod
    def get_stencil_choices():
        for folder in os.listdir(settings.STENCIL_PATH):
            if os.path.isdir(os.path.join(settings.STENCIL_PATH, folder)):
                yield (folder, folder)

    def __init__(self, *args, **kwds):
        super(StencilForm, self).__init__(*args, **kwds)
        self.fields['stencil'] = forms.ChoiceField(label='Stencil', choices=StencilForm.get_stencil_choices())