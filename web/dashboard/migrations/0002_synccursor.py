# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SyncCursor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cursor', models.CharField(max_length=255)),
                ('hash', models.CharField(max_length=255)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('website', models.ForeignKey(to='dashboard.WebSite')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
