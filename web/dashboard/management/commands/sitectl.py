from common.site_service import SiteService
from dashboard.models import WebSite
from dashboard.tasks import sync_site, preview_site, publish_site
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Site management from CLI"
    args = "ls|sync|preview [site_id]"

    def handle(self, *args, **options):
        if not args:
            raise CommandError("Usage manage.py sitectl subcommand")
        command = args[0]
        if command == "ls":
            for site in WebSite.objects.all():
                self.stdout.write("{pk}: [{domain}]\t\t [OK]".format(pk=site.pk, domain=site.domain))

        if command == "sync":
            if len(args) < 2:
                raise CommandError("Specify site id for sync")
            site_id = args[1]
            sync_site(site_id)

        if command == "preview":
            if len(args) < 2:
                raise CommandError("Specify site id for preview")
            site_id = args[1]
            url = preview_site(site_id)
            return url

        if command == "publish":
            if len(args) < 2:
                raise CommandError("Specify site id for render")
            site_id = args[1]
            url = publish_site(site_id)
            return url
