from django.core.management.base import BaseCommand
from allauth.socialaccount.models import SocialApp
from django.contrib.sites.models import Site
import os


class Command(BaseCommand):
    help = "Import social app keys from ENV"

    def handle(self, *args, **options):
        # Dropbox
        dropbox_key = os.getenv('DROPBOX_KEY')
        dropbox_secret = os.getenv('DROPBOX_SECRET')
        app = SocialApp.objects.filter(provider='dropbox').first()
        if dropbox_key and dropbox_secret and not app:
            print 'Created dropbox app'
            app = SocialApp.objects.create(
                name='Dropbox',
                provider='dropbox',
                secret=dropbox_secret,
                client_id=dropbox_key,
            )
            app.sites = Site.objects.all()
            app.save()
