import json
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import SingleObjectMixin
import django_rq
from os.path import join

from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView, View, FormView, TemplateView
from django.core.urlresolvers import reverse_lazy
from django.conf import settings
from dropbox.rest import ErrorResponse

from forms import StencilForm
from models import WebSite
from tasks import sync_site, preview_site, publish_site, copy_stencil


class WebsiteAuthMixin(SingleObjectMixin):

    @classmethod
    def as_view(cls):
        return login_required(super(WebsiteAuthMixin, cls).as_view())

    def get_queryset(self):
        if self.request.user.is_anonymous():
            return WebSite.objects.none()
        else:
            return WebSite.objects.filter(user=self.request.user.pk)


class WebsiteCommand(WebsiteAuthMixin, View):

    def post(self, request, *args, **kwargs):
        job = self.get_job()
        return HttpResponseRedirect(reverse_lazy('job_status', args=['default']) + '?task_id=' + job.id)

    def get_job(self, *args):
        raise NotImplemented


class Frontpage(TemplateView):
    template_name = 'dashboard/frontpage.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous() and request.user.website_set.exists():
            return redirect('site_list')
        return super(Frontpage, self).get(request, *args, **kwargs)


class WebsiteList(ListView):
    model = WebSite

    def get_queryset(self):
        qs = super(WebsiteList, self).get_queryset()
        if self.request.user.is_anonymous():
            return qs.none()
        else:
            return qs.filter(user=self.request.user.pk)


class WebsiteCreate(CreateView):
    model = WebSite
    fields = ['domain', 'remote']

    def get_form(self, form_class):
        form = super(WebsiteCreate, self).get_form(form_class)
        form.instance.user = self.request.user
        return form

    def form_valid(self, form):
        response = super(WebsiteCreate, self).form_valid(form)
        form.instance.initialize_remote()
        return response


class WebsiteDetail(WebsiteAuthMixin, DetailView):
    model = WebSite

    def get_context_data(self, **kwargs):
        data = super(WebsiteDetail, self).get_context_data(**kwargs)
        remote = self.object.get_remote_instance()
        # check if folder exists
        try:
            remote.list().next()
        except (ErrorResponse, StopIteration) as e:
            data['empty'] = True
            data['form'] = StencilForm()
        # TODO: move to async code (AJAX handler)
        data['files'] = remote.list()
        return data


class WebsiteUpdate(WebsiteAuthMixin, UpdateView):
    model = WebSite


class WebsiteDelete(WebsiteAuthMixin, DeleteView):
    model = WebSite
    success_url = reverse_lazy('frontpage')

    def post(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class WebsiteSync(WebsiteCommand):
    def get_job(self):
        site = self.get_object()
        return sync_site.delay(site.pk)


class WebsitePreview(WebsiteCommand):
    def get_job(self):
        site = self.get_object()
        return preview_site.delay(site.pk)


class WebsitePublish(WebsiteCommand):
    def get_job(self):
        site = self.get_object()
        return publish_site.delay(site.pk)


class WebsiteEditFile(WebsiteAuthMixin, View):
    # TODO: to FormView

    def get(self, request, path, *args, **kwds):
        website = self.get_object()
        remote = website.get_remote_instance()
        content = remote.read_file(path)
        return render(request, 'dashboard/file_editor.html', {'path': path, 'content': content, 'object': website})

    def post(self, request, path, *args, **kwds):
        website = self.get_object()
        remote = website.get_remote_instance()
        remote.write_file(path, request.POST['content'])
        return redirect('site_detail', website.pk)


class WebsiteStencil(WebsiteAuthMixin, View):
    # TODO: to FormView

    def post(self, request, *args, **kwds):
        website = self.get_object()
        form = StencilForm(request.POST)
        if form.is_valid():
            src = join(settings.STENCIL_PATH, form.cleaned_data['stencil'])
            job = copy_stencil.delay(website.pk, src)
            return HttpResponseRedirect(reverse_lazy('job_status', args=['default']) + '?task_id=' + job.id)
        else:
            return HttpResponseBadRequest('Form not valid: {0}'.format(form.errors))


def job_status(request, queue):
    if 'task_id' in request.GET:
        q = django_rq.get_queue(queue)
        job = q.fetch_job(request.GET['task_id'])
        job_data = job.to_dict()
        del job_data['data']
        job_data['task_id'] = request.GET['task_id']
        job_data['result'] = job.result
        return JsonResponse(job_data)
    else:
        return HttpResponseBadRequest("specify task_id")


@csrf_exempt
def dropbox_webhook(request):
    if request.method == 'GET':
        return HttpResponse(request.GET['challenge'])
    if request.method == 'POST':
        data = json.loads(request.body)
        if 'delta' in data and 'users' in data['delta']:
            for uid in data['delta']['users']:
                pass
        return HttpResponse('OK')
