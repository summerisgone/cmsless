from django.core.exceptions import ObjectDoesNotExist
import os
from shutil import rmtree
from os.path import join, dirname
from common.site_service import SiteService
from sync.base import BaseSync
from utils import ensure_dir, hash_folder


class LocalSync(BaseSync):
    def __init__(self, website):
        super(LocalSync, self).__init__(website)
        self._content_path = SiteService.get_content_path(self.website)

    def makedir(self, folder):
        os.mkdir(join(self._content_path, folder))

    def write_file(self, path, content):
        filename = join(self._content_path, path)
        ensure_dir(dirname(filename))
        with open(filename, 'w') as f:
            f.write(content)

    def sync(self):
        super(LocalSync, self).sync()

    def cleanup(self):
        if os.path.exists(self._content_path):
            rmtree(self._content_path)

    def delete(self, path):
        path_to_delete = join(self._content_path, path)
        try:
            if os.path.exists(path_to_delete):
                if os.path.isdir(path_to_delete):
                    rmtree(path_to_delete)
                else:
                    os.remove(path_to_delete)
        finally:
            return True

    def get_cursor(self):
        try:
            cursor = self.website.synccursor_set.get(hash=self.get_local_hash())
            return cursor.cursor
        except ObjectDoesNotExist:
            return None

    def get_local_hash(self):
        return hash_folder(self._content_path)
