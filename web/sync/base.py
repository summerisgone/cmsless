import logging
from common.site_service import SiteService


class BaseSync(SiteService):

    def __init__(self, website):
        super(BaseSync, self).__init__(website)
        self._remote = website.get_remote_instance()

    def sync(self):
        cursor = self.get_cursor()
        # TODO: work with prefix
        prefix = '/'
        if cursor is None:
            self.cleanup()  # we can cleanup if anyway we fetch all data
        cursor_and_files = self._remote.sync(cursor, prefix=prefix)
        cursor = cursor_and_files.next()
        logging.info('cursor: {0}'.format(cursor))
        something_changed = False
        for file_obj in cursor_and_files:
            something_changed = True
            if 'deleted' in file_obj and file_obj['deleted']:
                self.delete(file_obj['path'].lstrip('/'))
            else:
                content = self._remote.read_file(file_obj['path'])
                self.write_file(file_obj['path'].lstrip('/'), content)
        if something_changed:
            self._save_cursor(cursor)

    def cleanup(self):
        raise NotImplemented

    def makedir(self, param):
        raise NotImplemented

    def write_file(self, path, content):
        raise NotImplemented

    def delete(self, path):
        raise NotImplemented

    def get_local_hash(self):
        raise NotImplemented

    def get_cursor(self):
        return None

    def _save_cursor(self, cursor):
        self.website.synccursor_set.create(cursor=cursor, hash=self.get_local_hash())
