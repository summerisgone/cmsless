import logging
from common.site_service import SiteService
import os
from os.path import join
import shutil
from subprocess import Popen, PIPE
from publisher.base import BasePublisher, NoPreviewException

S3_WEBSITE_CFG = """s3_id: {s3_access_key}
s3_secret: {s3_secret}
s3_bucket: {domain}
s3_reduced_redundancy: true
site: .
exclude_from_upload:
   - s3_website.yml
"""


class AmazonPublisher(BasePublisher):
    s3_access_key = os.getenv('access_key')
    s3_secret = os.getenv('secret_key')

    def __create_deploy_config(self, src):
        with open(join(src, 's3_website.yml'), 'w') as cfg:
            cfg.write(S3_WEBSITE_CFG.format(**{
                's3_access_key': self.s3_access_key,
                's3_secret': self.s3_secret,
                'domain': self.website.domain
            }))

    def publish(self, src):
        if not self.has_preview():
            raise NoPreviewException("No preview found in preview path")
        self.__create_deploy_config(src)
        logging.getLogger().debug("try to publish from {0}".format(src))
        apply_proc = Popen("s3_website cfg apply --no-autocreate-cloudfront-dist --headless",
                           shell=True,
                           stdin=PIPE,
                           stdout=PIPE,
                           stderr=PIPE,
                           cwd=src)
        out, err = apply_proc.communicate()
        if err != '':
            logging.getLogger().error(err)
        if apply_proc.returncode != 0:
            raise Exception(
                "s3_website cfg apply running error: {0} {1} {2} {3}".format("s3_website cfg apply", out, err,
                                                                             apply_proc.returncode))

        push_proc = Popen("s3_website push",
                          shell=True,
                          stdin=PIPE,
                          stdout=PIPE,
                          stderr=PIPE,
                          cwd=src)
        out, err = push_proc.communicate()
        if err != '':
            logging.getLogger().error(err)
        if push_proc.returncode != 0:
            raise Exception(
                "s3_website push running error: {0}-{1}-{2}-{3}".format("s3_website push", out, err, push_proc.returncode))
        return 'http://{domain}.s3-website-us-east-1.amazonaws.com'.format(domain=self.website.domain)
