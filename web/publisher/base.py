from common.site_service import SiteService
from os.path import exists, isdir


class NoPreviewException(Exception):
    pass

class BasePublisher(SiteService):

    def has_preview(self):
        preview_path = SiteService.get_preview_path(self.website)
        return exists(preview_path) and isdir(preview_path)

    def publish(self, src):
        """Returns public url"""
        raise NotImplementedError
