from common.site_service import SiteService
import os
import shutil
from publisher.base import BasePublisher


class LocalPublisher(BasePublisher):

    def publish(self, src):
        if not os.path.exists(src):
            raise Exception("Nothing to publish, {0} does not exists".format(src))

        dst = SiteService.get_preview_path(self.website)
        if os.path.exists(dst):
            shutil.rmtree(dst)
        shutil.copytree(src, dst)
        return SiteService.get_preview_url(self.website)
