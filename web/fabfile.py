import sys
import os
from fabric.api import *
from contextlib import contextmanager as _contextmanager

env.hosts = ['cmsless@chelly.summerisgone.com']
env.directory = '/opt/cmsless/'


@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


def deploy():
    if sys.platform == 'win32':
        local('\"C:\\Program Files\\7-Zip\\7z.exe\" a -x!*.pyc -x!places -x!*settings_local.py ../source.zip .')
    else:
        local('zip -r -x*.pyc -xmedia* ../source.zip .')
    put('../source.zip', '/tmp')
    with cd('/tmp'):
        run('unzip  -o source.zip -d /opt/cmsless/source')
    restart()


def requirements():
    put('../requirements.txt')
    run('pip install -r requirements.txt --user')

def collectstatic():
    with cd('/opt/cmsless/source'):
        run('python manage.py collectstatic --noinput')

def migrate():
    with cd('/opt/cmsless/source'):
        run('python manage.py migrate tree')


def syncdb():
    with cd('/opt/cmsless/source'):
        run('python manage.py syncdb')


def restart():
    run('pkill -TERM -f gunicorn -u cmsless')
