import dropbox

# Get your app key and secret from the Dropbox developer website
app_key = 's58nfmg0b0n5ni8'
app_secret = 'vruiswc9lvctdta'
app_token = None #'w6Ed5T-sYeIAAAAAAAASff2Q8OGmVhjDMiNxJj3G-FhK3t74N6sU0z4gtOf_S59k'


def main():
    if not app_token:
        flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
        # Have the user sign in and authorize this token
        authorize_url = flow.start()
        print '1. Go to: ' + authorize_url
        print '2. Click "Allow" (you might have to log in first)'
        print '3. Copy the authorization code.'
        code = raw_input("Enter the authorization code here: ").strip()
        # This will fail if the user enters an invalid authorization code
        access_token, user_id = flow.finish(code)

        print 'token: ', access_token
    else:
        access_token = app_token


    client = dropbox.client.DropboxClient(access_token)
    print 'linked account: ', client.account_info()

    with open('requirements.txt', 'r') as f:
        response = client.put_file('/requirements.txt', f)
    print "uploaded:", response

    folder_metadata = client.metadata('/')
    print "metadata:", folder_metadata

    f, metadata = client.get_file_and_metadata('/requirements.txt')
    print f.read()


if __name__ == '__main__':
    main()